/**
 * Определяет API для генерирования строк случайных символов.
 */
module dev.kalenchukov.string.rand
{
	requires org.jetbrains.annotations;
	requires dev.kalenchukov.alphabet;
	requires dev.kalenchukov.numeralsystem;

	exports dev.kalenchukov.string.rand;
}